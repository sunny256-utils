#!/bin/sh

#==============================================================================
# ft
# File ID: e2b4cbf2-baff-11e8-97b6-c75b54f489f8
#
# Send files from $url to $dest with rsync, retry until it's done.
#
# Author: Øyvind A. Holm <sunny@sunbase.org>
# License: GNU General Public License version 2 or later.
#==============================================================================

progname=ft
VERSION=0.4.1

rcfile=~/.ftrc

opt_append=0
opt_copy=0
opt_help=0
opt_quiet=0
opt_verbose=0
while test -n "$1"; do
	case "$1" in
	-a|--append) opt_append=1; shift ;;
	-c|--copy) opt_copy=1; shift ;;
	-h|--help) opt_help=1; shift ;;
	-q|--quiet) opt_quiet=$(($opt_quiet + 1)); shift ;;
	-v|--verbose) opt_verbose=$(($opt_verbose + 1)); shift ;;
	--version) echo $progname $VERSION; exit 0 ;;
	--) shift; break ;;
	*)
		if printf '%s\n' "$1" | grep -q ^-; then
			echo "$progname: $1: Unknown option" >&2
			exit 1
		else
			break
		fi
	break ;;
	esac
done
opt_verbose=$(($opt_verbose - $opt_quiet))

if test "$opt_help" = "1"; then
	test $opt_verbose -gt 0 && { echo; echo $progname $VERSION; }
	cat <<END

Usage: $progname [options] [ -- rsync_options ]

Options:

  -a, --append
    Don't check downloaded data, only append data to the end of files.
    Use with care.
  -c, --copy
    Don't use the --inplace option with rsync, write to a copy of the 
    destination file before overwriting it.
  -h, --help
    Show this help.
  -q, --quiet
    Be more quiet. Can be repeated to increase silence.
  -v, --verbose
    Increase level of verbosity. Can be repeated.
  --version
    Print version information.

Create the rcfile and define url (ssh address of source directory) and 
dest (local directory). For example:

url=user@fromhost:/home/user/tel/
dest=/sdcard/tel/

END
	exit 0
fi

cmd_rsync="rsync -rmvLPzz --timeout=15"

unset url dest
test -e "$rcfile" && . "$rcfile"

if test -z "$url" -o -z "$dest"; then
	echo $progname: url or dest not defined in $rcfile >&2
	exit 1
fi

if test "$opt_append" = "1"; then
	append_str="--append"
else
	append_str=""
fi
if test "$opt_copy" = "1"; then
	copy_str=""
else
	copy_str="--inplace"
fi
ps | grep -v "grep $cmd_rsync" | grep "$cmd_rsync" && exit 1
until $cmd_rsync $append_str $copy_str $url $dest "$@"; do
  date;
  sleep 2;
done

# vim: set ts=8 sw=8 sts=8 noet fo+=w tw=79 fenc=UTF-8 :
